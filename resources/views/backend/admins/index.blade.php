@extends('backend.layouts.master')
@section('after-script')
	{{ HTML::script('backend/js/users.js') }}
	{{ HTML::script('backend/js/permission.js') }}
@endsection
@section('main')
<div class="col-xs-12">
    @include('backend.includes.search', ['action' => route('admin.admins')])
    <div class="x_panel">
      <div class="x_title">
        <h2 class=""><i class="fa fa-filter"></i></h2>
        <div class="form-group col-xs-2">
            <select name="role" id="" class="form-control fillter_width_url">
                <option {{ (empty($params['role'])) ? 'selected' : '' }} value="{{ route('admin.admins') }}">All</option>
                @foreach ($roles as $role)
                  <option {{ ($params['role'] == strtolower($role->name)) ? 'selected' : '' }} value="{{ route('admin.admins', ['role' => strtolower($role->name)]) }}">{{ $role->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="clearfix"></div>
      </div>

      <div class="x_content">

        <div class="table-responsive">
          <table class="table table-striped jambo_table bulk_action">
            <thead>
              <tr class="headings">
                <th class="column-title">ID</th>
                <th class="column-title">Username</th>
                <th class="column-title">Email</th>
                <th class="column-title">Avatar</th>
                <th class="column-title">Role</th>
                <th class="column-title">Permissions</th>
                <th class="column-title">Created_at</th>
                <th class="column-title no-link last"><span class="nobr">Action</span></th>
              </tr>
            </thead>

            <tbody>
              @foreach ($admins as $key => $admin)
                <tr class="even pointer">
                    <td>{{ $admin->id }}</td>
                    <td>{{ $admin->username }}</td>
                    <td>{{ $admin->email }}</td>
                    <td>
                        <img src="{{ (!is_null($admin->image)) ? $admin->image : url('images/default.png') }}" alt="" width="60px" height="60px">
                    </td>
                    <td>
                        @foreach ($admin->roles as $role)
                            <p class="label label-default">{{ $role->name }}</p>
                        @endforeach
                    </td>
                    <td>
                        @foreach ($admin->roles as $role)
                        @foreach ($role->permissions as $permission)
                            <p>{{ $permission->name }}</p>
                        @endforeach
                        @endforeach
                    </td>
                    <td>{{ $admin->created_at->format('d-m-Y') }}</td>
                    <td class="last">
                        <div class="btn-group">
                        <a href="{{ route('admin.profile', $admin->id) }}" class="btn btn-sm btn-primary btn-view-users" data-id="{{ $admin->id }}"><i class="fa fa-eye"></i> View</a>
                        @if ($admin->id !== $guard->id && !$admin->isAdmin())
                          <button class="btn btn-sm btn-info btn-permission-admin" data-item="{{ $key }}" data-id="{{ $admin->id }}"><i class="fa fa-cog"></i> Permissions</button>
                          <button class="btn btn-sm btn-{{ ($admin->active) ? 'warning' : 'success' }} btn-user-active" data-id="{{ $admin->id }}" data-type="{{ !$admin->active }}">{{ (!$admin->active) ? 'Active' : 'Unactive' }}</button>
                          <button class="btn btn-sm btn-danger btn-user-block" data-id="{{ $admin->id }}"><i class="fa fa-lock"></i> Block</button>
                        @endif
                        </div>
                    </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
                
        <!--Phân trang-->
        @include('backend.includes.pagination', ['data' => $admins, 'appended' => ['search' => Request::get('search')]])
      </div>
    </div>
  </div>

  {{-- modal permissons --}}
  <div class="modal fade bs-example-modal modal-admins" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title" id="myModalLabel2">Permissions</h4>
        </div>
        <div class="modal-body">
          <ul class="to_do permissions">
            @foreach ($permissions as $permission)
              <li>
                <p><input type="checkbox" class="flat" name="{{ $permission->name }}" value="{{ $permission->id }}"> {{ $permission->name }} </p>
              </li>
            @endforeach
          </ul>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary btn-save-change-permission" data-id="">Save changes</button>
        </div>

      </div>
    </div>
  </div>
@endsection