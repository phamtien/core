<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        @if ($guard->hasRoles(['amdin']) || $guard->hasAccess(['manage user']))
        <h3>Administrator</h3>
        <ul class="nav side-menu">
            <li class="{{ Request::is('users') ? 'active' : '' }}"><a><i class="fa fa-user"></i> Administrator <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li class="{{ Request::is('users') ? 'active' : '' }}">
                        <a href="{{ route('admin.users') }}">Users</a>
                    </li>
                    <li class="{{ Request::is('admins') ? 'active' : '' }}">
                        <a href="{{ route('admin.admins') }}">Admins</a>
                    </li>
                </ul>
            </li>
        </ul>
        @endif
    </div>
        
</div>