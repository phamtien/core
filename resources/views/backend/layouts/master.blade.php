<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="{{url('favicon.ico')}}" type="image/ico" />

        <title>Admin-Pipeline</title>

        <!-- Bootstrap -->
        <link href="{{url('vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="{{url('vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="{{url('build/css/custom.min.css')}}" rel="stylesheet">

        <meta name="csrf-token" content="{{ csrf_token() }}" />
        {{-- after css --}}
        @yield('after-css')
    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                {{-- nav --}}
                @include('backend.includes.nav')

                <!-- top header -->
                @include('backend.includes.header')

                <!-- page content -->
                <div class="right_col" role="main">
                    @yield('main')
                </div>

                <!-- footer content -->
                @include('backend.includes.footer')
            </div>
        </div>
        @include('includes.partials.params')

        <!-- jQuery -->
        <script src="{{url('vendors/jquery/dist/jquery.min.js')}}"></script>
        <!-- Bootstrap -->
        <script src="{{url('vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
        <script src="{{url('vendors/bootstrap-notify/bootstrap-notify.min.js')}}"></script>

        {{-- before script --}}
        @yield('before-script')
        <!-- Custom Theme Scripts -->
        <script src="{{url('build/js/custom.min.js')}}"></script>
        <script src="{{url('backend/js/common.js')}}"></script>
        {{-- after script --}}
        @yield('after-script')
    </body>
</html>
