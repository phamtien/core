@extends('backend.layouts.master')
@section('after-script')
    {{ HTML::script('backend/js/users.js') }}
@endsection
@section('main')
@include('backend.includes.previous', ['back_link' => URL::previous()])
<div class="row">
  <div class="col-xs-12">
    <div class="col-xs-4">
      <div class="x_panel">
        <div class="x_content">
          <div class="col-xs-12 profile_left">
            <div class="profile_img">
              <div id="crop-avatar">
                <!-- Current avatar -->
                <img class="img-responsive avatar-view" src="{{ (!is_null($user->image)) ? $user->image : url('images/default.png') }}" alt="Avatar" title="Change the avatar">
              </div>
            </div>
            <h3>{{ $user->username }}</h3>

            <ul class="list-unstyled user_data">
              <li><i class="fa fa-envelope-o"></i> {{ $user->email }}
              </li>
              <li><i class="fa fa-calendar"></i> {{ $user->created_at->format("d-m-Y") . " ( " . $user->created_at->diffForHumans() ." )" }}
              </li>
            </ul>

            <a class="btn btn-danger btn-user-block" data-id="{{ $user->id }}"><i class="fa fa-lock"></i> Block</a>
            <br />

          </div>
        </div>
      </div>
    </div>
    <div class="col-xs-8">
      <div class="x_panel">
        <div class="x_title">
          <h2>Account</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">

          <!-- start form for validation -->
          <form id="demo-form" data-parsley-validate>
            <label for="fullname">Name * :</label>
            <input type="text" id="username" class="form-control" name="username" value="{{ $user->username }}" required />

            <label for="email">Email * :</label>
            <input type="email" id="email" class="form-control" name="email" value="{{ $user->email }}" disabled />
            
            <label for="email">Password * :</label>
            <input type="password" id="password" class="form-control" name="password" />
            
            <br/>
            <a class="btn btn-success btn-user-update">Update</a>
          </form>
          <!-- end form for validations -->

        </div>
      </div>
    </div>
  </div>
</div>
@endsection