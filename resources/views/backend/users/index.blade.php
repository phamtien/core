@extends('backend.layouts.master')
@section('after-script')
	{{ HTML::script('backend/js/users.js') }}
@endsection
@section('main')
<div class="col-xs-12">
    @include('backend.includes.search', ['action' => route('admin.users')])
    <div class="x_panel">
      <div class="x_title">
        <h2 class=""><i class="fa fa-filter"></i></h2>
        <div class="clearfix"></div>
      </div>

      <div class="x_content">

        <div class="table-responsive">
          <table class="table table-striped jambo_table bulk_action">
            <thead>
              <tr class="headings">
                <th class="column-title">ID</th>
                <th class="column-title">Username</th>
                <th class="column-title">Email</th>
                <th class="column-title">Avatar</th>
                <th class="column-title">Created_at</th>
                <th class="column-title no-link last"><span class="nobr">Action</span></th>
              </tr>
            </thead>

            <tbody>
              @foreach ($users as $user)
                <tr class="even pointer">
                  <td>{{ $user->id }}</td>
                  <td>{{ $user->username }}</td>
                  <td>{{ $user->email }}</td>
                  <td>
                    <img src="{{ (!is_null($user->image)) ? $user->image : url('images/default.png') }}" alt="" width="60px" height="60px">
                  </td>
        				  <td>{{ $user->created_at->format('d-m-Y') }}</td>
                  <td class=" last">
                    <div class="btn-group">
                      <a href="{{ route('admin.profile', $user->id) }}" class="btn btn-sm btn-primary btn-view-users" data-id="{{ $user->id }}"><i class="fa fa-eye"></i> View</a>
                      @if ($user->id !== $guard->id)
                        <button class="btn btn-sm btn-danger btn-user-block" data-id="{{ $user->id }}"><i class="fa fa-lock"></i> Block</button>
                      @endif
                    </div>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
                
        <!--Phân trang-->
        @include('backend.includes.pagination', ['data' => $users, 'appended' => ['search' => Request::get('search')]])
      </div>
    </div>
  </div>
@endsection