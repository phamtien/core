<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel Core
<p>version: 5.8</p>
<p>PHP >= 7.1.3</p>

Các kiến thức cần có:
- Lập trình hướng đối tượng OOP trong PHP
- Repository Pattern trong laravel
- Traits
- Transformers trong laravel
- Request
- Policy
- Middleware

Project core chia làm 2 phần: Dashborad và API

## Khỏi tạo project 

<p>git clone </p>
<p>cd core</p>
<p>cấu hình admin và password trên file .env</p>
<p>cấu hình domain cho project.</p>
<p>chạy lệnh: </p>

- composer install
- php artisan migrate --seed
- php artisan jwt:secret

Tạo thư mục upload file images

- public/uploads/images

## Dashboard

URL: core.local/dashboard

Trang quản lý nội dung bao gồm:
- Phân quyền truy cập


## API

Api sử dụng thư viện JWT-Auth
Các api tạo sẵn:
- POST: http://core.local/api/login
- POST: http://core.local/api/login/{provider} (login qua facebook/google)
- GET: http://core.local/api/logout
- GET: http://core.local/api/me
- POST: http://core.local/api/register
- GET: http://core.local/api/users