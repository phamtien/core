<?php

namespace App\Repositories\Users\Contract;

use App\Core\Repositories\Contract\BaseRepositoryInterface;

interface UserRepositoryInterface extends BaseRepositoryInterface {

    public function getUsersWithPaginate(array $params);
    
    public function loginSocial(string $provider, string $token);
}