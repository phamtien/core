<?php

namespace App\Repositories\Users;

use App\Core\JsonResponse;
use App\Core\Repositories\BaseRepository;
use App\Http\Resources\User\UserResource;
use App\Models\Users\SocialAccount;
use App\Repositories\Users\Contract\UserRepositoryInterface;
use App\User;
use Exception;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Response;

class UserRepository extends BaseRepository implements UserRepositoryInterface {

    protected $model;
    protected $client;
    protected $social;

    public function __construct(User $user, SocialAccount $social)
    {
        parent::__construct($user);
        $this->model = $user;
        $this->social = $social;
        $this->client = new Client();
    }

    public function getUsersWithPaginate(array $params)
    {
        $users = $this->model->query()->whereAdmin(false);
        if(!empty($params['search'])){
            $search = strtolower($params['search']);
            $users->where(function($q) use($search){
                $q->where('username', 'like', "%$search%");
                $q->orWhere('email', 'like', "%$search%");
                $q->orWhere('id', 'like', "%$search%");
            });
        }
        /**
         * Format variable order: key => value
         */
        if(!empty($params['order'])){
            foreach($params['order'] as $order => $sort){
                $users->orderBy($order, $sort);
            }
        }else{
            $users->latest();
        }

        return UserResource::collection($users->paginate($params['limit']));
    }

    public function loginSocial(string $provider, string $accessToken)
    {
        $url = '';
        switch ($provider) {
            case "google":
                $url = "https://www.googleapis.com/oauth2/v3/userinfo?access_token=$accessToken";
                break;
            case "facebook":
                $url = "https://graph.facebook.com/v3.1/me?fields=id,name,email,picture&access_token=$accessToken";
                break;
        }
        if(!empty($url)){
            try{
                $token = $this->client->get($url);
                $profile = json_decode($token->getBody()->getContents(), true);
                if($profile){
                    $params = [];
                    switch($provider){
                        case "google":
                            $params = [
                                'provider'     => $provider,
                                'provider_id'  => $profile['sub'],
                                'email'        => $profile['email'],
                                'username'     => $profile['name'],
                                'image'        => $profile['picture']
                            ];
                            break;
                        case "facebook":
                            $params = [
                                'provider'    => $provider,
                                'provider_id' => $profile['id'],
                                'email'       => $profile['email'] ?? null,
                                'username'    => $profile['name'],
                                'image'       => $profile['picture']['data']['url'] ?? null
                            ];
                            break;
                    }
                    return $this->findOrCreateUserSocial($params);
                }
                return $profile;
            }catch(\Exception $e){
                throw new Exception(response()->json(new JsonResponse([], $e->getMessage()), Response::HTTP_INTERNAL_SERVER_ERROR));
            }
        }else{
            return false;
        }
    }

    /**
     * Create or get user while exists within social
     */
    public function findOrCreateUserSocial(array $data)
    {
        if(empty($data)){
            return false;
        }
        $socialAccount = [
            'provider'    => $data['provider'],
            'provider_id' => $data['provider_id'],
        ];
        $account = $this->social->where($socialAccount)->first();
        if($account){
            return $account->user;
        }else{
            $user = User::whereEmail($data['email'])->first();
            if(!$user){
                $user = User::create([
                    'email'    => $data['email'],
                    'username' => $data['username'],
                    'image'    => $data['image'],
                    'password' => null
                ]);
            }
            $user->socials()->create($socialAccount);
            return $user;
        }
    }
}