<?php

namespace App\Repositories\Admins;

use App\Core\Repositories\BaseRepository;
use App\Http\Resources\Admin\AdminResource;
use App\Models\Admins\Admin;
use App\Repositories\Admins\Contract\AdminRepositoryInterface;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AdminRepository extends BaseRepository implements AdminRepositoryInterface {

    protected $model;

    public function __construct(Admin $admin)
    {
        parent::__construct($admin);
        $this->model = $admin;
    }

    /**
     * @param Collection $collection
     * @return void
     */
    public function saveImageAdmin($file)
    {
        $filename = 'Thumb_image_' . time() . '.' . $file->getClientOriginalExtension();
        $path = $this->saveImage($file, $filename);
        return $this->model->update([
            'image' => $path
        ]);
    }

    public function changePermissions(array $permissions)
    {
        /**
         * Check if not administrator then update permission
         */
        self::checkIfNotAdmin();

        foreach($this->model->roles as $role){
            $role->permissions()->sync($permissions);
        }
        return true;
    }

    public function activeAdmin(bool $type)
    {
        self::checkIfNotAdmin();

        if($this->model->update(['active' => $type])){
            return true;
        }
        return false;
    }

    public function getAdminsWithPaginate(array $params)
    {
        $admins = $this->model->query()->whereAdmin(true)->with(['roles' => function($q){
            $q->with('permissions');
        }]);
        if (!empty($params['role'])) {
            $role = strtolower($params['role']);
            $admins->whereHas('roles', function ($q) use ($role) {
                $q->whereName($role);
            });
        }
        if(!empty($params['search'])){
            $search = strtolower($params['search']);
            $admins->where(function($q) use($search){
                $q->where('username', 'like', "%$search%");
                $q->orWhere('email', 'like', "%$search%");
                $q->orWhere('id', 'like', "%$search%");
            });
        }
        /**
         * Format variable order: key => value
         */
        if(!empty($params['order'])){
            foreach($params['order'] as $order => $sort){
                $admins->orderBy($order, $sort);
            }
        }else{
            $admins->latest();
        }

        return AdminResource::collection($admins->paginate($params['limit']));
    }

    /**
     * Check if not administrator
     */
    private function checkIfNotAdmin(){
        if($this->guard()->id == $this->model->id){
            throw new HttpException(Response::HTTP_UNAUTHORIZED, 'Unauthorization');
        }
    }
 
}