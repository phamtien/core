<?php

namespace App\Repositories\Admins\Contract;

use App\Core\Repositories\Contract\BaseRepositoryInterface;
use Illuminate\Http\UploadedFile;

interface AdminRepositoryInterface extends BaseRepositoryInterface {

    public function saveImageAdmin($file);

    public function changePermissions(array $permissions);

    public function activeAdmin(bool $type);

    public function getAdminsWithPaginate(array $params);
}