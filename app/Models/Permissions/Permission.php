<?php

namespace App\Models\Permissions;

use App\Models\Roles\Role;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $guarded = [];

    protected $fillable = [
        'name',
        'slug'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * Relationships for premission
     */
    public function roles(){
        return $this->belongsToMany(Role::class);
    }

    public function setNameAttribute($name){
        $this->attributes['name'] = strtolower($name);
    }
    
    public function getNameAttribute(){
        return ucwords($this->attributes['name']);
    }
}
