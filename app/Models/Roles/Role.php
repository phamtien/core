<?php

namespace App\Models\Roles;

use App\Models\Permissions\Permission;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
    protected $guarded = [];

    protected $fillable = [
        'name',
        'slug'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * Relationship for role
     */
    public function permissions(){
        return $this->belongsToMany(Permission::class);
    }

    public function setNameAttribute($name){
        $this->attributes['name'] = strtolower($name);
    }
    
    public function getNameAttribute(){
        return ucwords($this->attributes['name']);
    }

    public function hasPermissions(array $permissions){
        // Get all permissons in role
        $roleHasPermissions = array_map(function($permission){
            return $permission['name'];
        }, $this->permissions->toArray());
        foreach($permissions as $permission){
            if(in_array(ucwords($permission), $roleHasPermissions)){
                return true;
            }
        }
        return false;
    }

}
