<?php

namespace App\Models\Admins;

use App\Core\Acl;
use App\Models\Roles\Role;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User;

class Admin extends User
{
    use SoftDeletes;

    const ACTIVE = true;
    const UNACTIVE = false;
    const ADMIN = true;

    protected $table = 'users';
    protected $guarded = [];

    protected $fillable = [
        'username',
        'email',
        'password',
        'image',
        'admin',
        'active'
    ];

    protected $hidden = [
        'password',
        'remember_token',
        'created_at',
        'updated_at',
    ];

    /**
     * Relationships for admin
     */
    public function roles(){
        return $this->belongsToMany(Role::class);
    }

    protected function setUsernameAttribute($username){
        $this->attributes['username'] = strtolower($username);
    }

    protected function getUsernameAttribute(){
        return ucwords($this->attributes['username']);
    }

    public function getAllPermissions(){
        return $this->load('roles')->roles->flatMap(function($role){
            return $role->permissions;
        })->sort()->values();
    }

    public function hasAccess(array $permissions){
        // Check if the permission is available in any role
        foreach($this->roles as $role){
            if($role->hasPermissions($permissions)){
                return true;
            }
        }
        return false;
    }

    public function hasRoles(array $roles){
        $rolesForAdmin = array_map(function($role){
            return $role['name'];
        }, $this->roles->toArray());
        foreach($roles as $role){
            if(in_array(ucwords($role), $rolesForAdmin)){
                return true;
            }
        }
        return false;
    }

    public function isAdmin(){
        return $this->hasRoles([Acl::ROLE_ADMIN]);
    }
}
