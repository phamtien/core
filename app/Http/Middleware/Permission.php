<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Permission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $route = $request->route();
        $actions = $route->getAction();
        $admin = Auth::guard('admin')->user();
        if(!$admin->hasAccess($actions['permissions'])){
            return redirect()->back();
        }
        return $next($request);
    }
}
