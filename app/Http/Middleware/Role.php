<?php

namespace App\Http\Middleware;

use Closure;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $route = $request->route();
        $actions = $route->getAction();
        $admin = Auth::guard('admin')->user();
        if(!$admin->hasRoles($actions['roles'])){
            return redirect()->back();
        }
        return $next($request);
    }
}
