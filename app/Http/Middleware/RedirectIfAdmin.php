<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class RedirectIfAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'admin')
    {
        if(!auth()->guard($guard)->check()){
            return redirect('/dashboard');
        }
        $admin = Auth::guard($guard)->user();
        View::share('guard', $admin);
        return $next($request);
    }
}
