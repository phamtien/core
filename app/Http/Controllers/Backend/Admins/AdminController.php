<?php

namespace App\Http\Controllers\Backend\Admins;

use App\Core\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ImageRequest;
use App\Models\Admins\Admin;
use App\Repositories\Admins\AdminRepository;
use App\Repositories\Admins\Contract\AdminRepositoryInterface;
use App\Repositories\Permissions\Contract\PermissionRepositoryInterface;
use App\Repositories\Roles\Contract\RoleRepositoryInterface;
use Illuminate\Support\Arr;
use JavaScript;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends Controller
{
    const ITEM_PER_PAGE = 20;

    private $admin;
    private $role;
    private $permission;

    public function __construct(AdminRepositoryInterface $admin, RoleRepositoryInterface $role, PermissionRepositoryInterface $permission)
    {
        $this->admin = $admin;
        $this->role  = $role;
        $this->permission  = $permission;
    }

    /**
     * Update image
     * @param File $file
     */
    public function updateImageAdmin(ImageRequest $request){
        if($request->hasFile('image')){
            $admin = $this->admin->find($request->id);
            $adminRepo = new AdminRepository($admin);
            $adminRepo->saveImageAdmin($request->file('image'));
        }
        return redirect()->back();
    }

    public function profile(Request $request, Admin $admin){
        JavaScript::put([
            'link_back' => route('admin.admins')
        ]);
        
        $view = view('backend.admins.profile');
        $view->with('admin', $admin);
        return $view;
    }

    /**
     * Update permission for admin
     * @param int $id
     * @param array $permissions
     */
    public function updatePermission(Request $request){
        $permissions = (isset($request->permissions)) ? $request->permissions : [];
        $admin = $this->admin->find($request->id);
        $adminRepo = new AdminRepository($admin);
        if($adminRepo->changePermissions($permissions)){
            return response(new JsonResponse(['success']), Response::HTTP_NO_CONTENT);
        }
        return response(new JsonResponse([], 'Update permission error'), Reponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function active(Request $request){
        $params = [
            'id' => $request->id,
            'type' => (is_null($request->type)) ? false : true
        ];
        $admin = $this->admin->find($params['id']);
        $adminRepo = new AdminRepository($admin);
        if($adminRepo->activeAdmin($params['type'])){
            $result = ($params['type']) ? 'Tài khoản đã được kích hoạt.' : 'Tài khoản đã được tạm khoá.';
            return response(new JsonResponse([$result]), Response::HTTP_OK);
        }
        return response(new JsonResponse([], 'Cannot active'), Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function index(Request $request){
        $all = $request->all();
        $params['limit'] = Arr::get($all, 'limit', static::ITEM_PER_PAGE);
        $params['search'] = Arr::get($all, 'search', '');
        $params['role'] = Arr::get($all, 'role', '');

        $admins = $this->admin->getAdminsWithPaginate($params);
        $roles = $this->role->all();
        $permissions = $this->permission->all();
        
        JavaScript::put([
            'link_admin_active' => route('admin.admins.active'),
            'link_update_permission' => route('admin.admins.updatePermission'),
            'admins' => $admins
        ]);

        $view = view('backend.admins.index');
        $view->with('admins', $admins);
        $view->with('roles', $roles);
        $view->with('permissions', $permissions);
        $view->with('params', $params);

        return $view;
    }
}
