<?php

namespace App\Http\Controllers\Backend\Users;

use App\Core\Acl;
use App\Core\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\User\UserResource;
use App\Repositories\Users\Contract\UserRepositoryInterface;
use App\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use JavaScript;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    const ITEM_PER_PAGE = 20;

    private $user;

    public function __construct(UserRepositoryInterface $user)
    {
        $this->user = $user;
    }

    public function delete(Request $request){
        $id = (int)$request->id;
        $admin = Auth::guard('admin')->user();
        // Check permission has delete
        if($admin->hasAccess([Acl::PERMISSION_USER_MANAGE]) && ($id !== $admin->id)){
            $this->user->find($id)->delete();
            return response(null, Response::HTTP_NO_CONTENT);
        }
        return response(new JsonResponse([], 'You do not have permission to delete users'), Response::HTTP_UNAUTHORIZED);
    }

    public function profile(Request $request, User $user){
        JavaScript::put([
            'link_user_delete' => route('admin.users.delete'),
            'link_back' => route('admin.users')
        ]);
        $user = new UserResource($user);
        
        $view = view('backend.users.profile');
        $view->with('user', $user);
        return $view;
    }

    public function index(Request $request){
        JavaScript::put([
            'link_user_delete' => route('admin.users.delete'),
            'link_back' => route('admin.users')
        ]);
        $all = $request->all();
        $params['limit'] = Arr::get($all, 'limit', static::ITEM_PER_PAGE);
        $params['search'] = Arr::get($all, 'search', '');

        $users = $this->user->getUsersWithPaginate($params);
        $view = view('backend.users.index');
        $view->with('users', $users);
        return $view;
    }

}
