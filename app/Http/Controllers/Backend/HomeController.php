<?php

namespace App\Http\Controllers\Backend;

use App\Core\Acl;
use App\Core\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Users\CreateUserRequest;
use App\Http\Requests\Users\LoginRequest;
use App\Http\Resources\Admin\AdminResource;
use App\Models\Admins\Admin;
use App\Repositories\Admins\Contract\AdminRepositoryInterface;
use App\Repositories\Roles\Contract\RoleRepositoryInterface;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use JavaScript;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends Controller
{
    use AuthenticatesUsers;

    private $admin;
    private $role;

    public function __construct(AdminRepositoryInterface $admin, RoleRepositoryInterface $role)
    {
        $this->middleware('admin')->only('logout');
        $this->admin = $admin;
        $this->role = $role;
    }

    public function index(){
        JavaScript::put([
            'register_link' => route('admin.register')
        ]);
        $roles = Acl::roles([Acl::ROLE_ADMIN]);
        $view = view('backend.login');
        $view->with('roles', $roles);
        return $view;
    }

    public function login(LoginRequest $request){
        $credentials = $request->only('email', 'password');
        $credentials['admin'] = Admin::ADMIN;
        $credentials['active'] = Admin::ACTIVE;
        if($this->guard()->attempt($credentials)){
            return redirect()->route('admin.dashboard');
        }
        return redirect()->back()->withErrors(['msg' => 'Account does not have access!']);
    }

    /**
     * Register admin
     */
    public function register(CreateUserRequest $request){
        $params = $request->only('email', 'password', 'username', 'admin');
        $params['password'] = bcrypt($params['password']);
        try{
            $admin = $this->admin->create($params);
            $role = $this->role->findRoleName($request->role);
            $admin->roles()->attach($role);
            return response()->json(new AdminResource($admin), Response::HTTP_CREATED);
        }catch(\Exception $e){
            return response()->json(new JsonResponse([], $e->getMessage()), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function logout(){
        $this->guard()->logout();
        return redirect('/dashboard');
    }

    private function guard(){
        return Auth::guard(Acl::GUARD_ADMIN);
    }
}
