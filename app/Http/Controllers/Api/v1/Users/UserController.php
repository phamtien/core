<?php

namespace App\Http\Controllers\Api\v1\Users;

use App\Core\Acl;
use App\Core\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Users\CreateUserRequest;
use App\Http\Requests\Users\LoginRequest;
use App\Http\Resources\User\UserResource;
use App\Repositories\Users\Contract\UserRepositoryInterface;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    const ITEM_PER_PAGE = 20;

    private $user;

    public function __construct(UserRepositoryInterface $user)
    {
        $this->user = $user;
    }

    /**
     * Get list users with paginate
     * @param int $page
     * @param int $limit
     */
    public function getUsers(Request $request){
        $all = $request->all();
        $params['limit'] = Arr::get($all, 'limit', static::ITEM_PER_PAGE);
        $params['search'] = Arr::get($all, 'search', '');

        $users = $this->user->getUsersWithPaginate($params);
        return response(new JsonResponse($users), Response::HTTP_OK);
    }

    /**
     * Login without facebook, google
     */
    public function loginWithSocial(Request $request, $provider){
        $access = $request->token;
        $user = $this->user->loginSocial($provider, $access);
        if($user && $token = $this->guard()->login($user)){
            return response()->json(new UserResource($this->guard()->user()), Response::HTTP_OK)->header('Authorization', $token);
        }
        return response()->json(new JsonResponse([], 'Login error'), Response::HTTP_UNAUTHORIZED);
    }
    
    public function login(LoginRequest $request){
        $credentials = $request->only('email', 'password');
        if($token = $this->guard()->attempt($credentials)){
            return response()->json(new UserResource($this->guard()->user()), Response::HTTP_OK)->header('Authorization', $token);
        }
        return response()->json(new JsonResponse([], 'Login error'), Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Register user with email
     * @param string $email
     * @param string $password
     */
    public function register(CreateUserRequest $request){
        $params = $request->only('username', 'email', 'password');
        $params['password'] = bcrypt($params['password']);
        try{
            $user = $this->user->create($params);
            return response()->json(new UserResource($user), Response::HTTP_CREATED);
        }catch(\Exception $e){
            return response()->json(new JsonResponse([], $e->getMessage()), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function logout()
    {
        $this->guard()->logout();
        return response()->json((new JsonResponse())->success([]), Response::HTTP_OK);
    }

    public function me(){
        return new UserResource($this->guard()->user());
    }

    /**
     * @return mixed
     */
    private function guard(){
        return Auth::guard(Acl::GUARD_API);
    }
}
