<?php

namespace App\Http\Requests\Users;

use App\Http\Requests\BaseFromRequest;
use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends BaseFromRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:6',
            'username' => 'required|min:3'
        ];
    }
}
