<?php

namespace App\Core\Traits;

use App\Core\Acl;
use Illuminate\Support\Facades\Auth;

trait Authorization {
    
    /**
     * Get guard admin
     * @return Model $guard
     */
    public function guard(){
        return Auth::guard(Acl::GUARD_ADMIN)->user();
    }

}