<?php
namespace App\Core;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;

final class Acl
{
    const GUARD_WEB     = 'web';
    const GUARD_ADMIN   = 'admin';
    const GUARD_API     = 'api';

    const ROLE_ADMIN    = 'administrator';
    const ROLE_ADMODE   = 'admode';
    const ROLE_VISITOR  = 'visitor';
    const ROLE_USER     = 'user';
    const ROLE_EDITOR   = 'editor';

    const PERMISSION_USER_MANAGE        = 'manage user';
    const PERMISSION_PERMISSION_MANAGE  = 'manage permission';
    const PERMISSION_ADMODE             = 'manage admode';
    const PERMISSION_ADMINISTRATOR      = 'manage administrator';

    const PERMISSION_VIEW_DASHBOARD             = 'view dashboard';
    const PERMISSION_VIEW_MENU_ADMINISTRATOR    = 'view menu administrator';

    /**
     * Get permissions exclude some permissions from the list
     * @return array
     */
    public static function permissions(array $exclusives = [])
    {
        try {
            $class = new \ReflectionClass(__CLASS__);
            $constants = $class->getConstants();
            $permissions = Arr::where($constants, function ($value, $key) use ($exclusives) {
                return !in_array($value, $exclusives) && Str::startsWith($key, 'PERMISSION_');
            });

            return array_values($permissions);
        } catch (\ReflectionException $e) {
            return [];
        }
    }

    /**
     * Get menu permissions
     * @return array
     */
    public static function menuPermissions()
    {
        try {
            $class = new \ReflectionClass(__CLASS__);
            $constants = $class->getConstants();
            $permissions = Arr::where($constants, function ($value, $key) {
                return Str::startsWith($key, 'PERMISSION_VIEW_MENU_');
            });

            return array_values($permissions);
        } catch (\ReflectionException $e) {
            return [];
        }
    }

    /**
     * Get roles exclude some roles from the list
     * @return array
     */
    public static function roles(array $exclusives = [])
    {
        try {
            $class = new \ReflectionClass(__CLASS__);
            $constants = $class->getConstants();
            $roles = Arr::where($constants, function ($value, $key) use ($exclusives) {
                return !in_array($value, $exclusives) && Str::startsWith($key, 'ROLE_');
            });

            return array_values($roles);
        } catch (\ReflectionException $e) {
            return [];
        }
    }

}
