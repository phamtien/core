$(function() {
    /**
     * Process for admin manage
     */
    $('.btn-user-active').click(function(){
        var id = $(this).data('id');
        var type = $(this).data('type');
        $.ajax({
            type: 'POST',
            url: link_admin_active,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: {
                id: id,
                type: type,
                _method: 'PUT'
            },
            success:function(res){
                $.notify({
                    message: res.data[0]
                },{
                    type: 'success'
                });
                setTimeout(function(){
                    window.location.reload(1)
                }, 2000);
            },
            error: function(e){
                console.log(e);
            }
        });
        
    })

     //-----------------------------------

    // Block user
    $('.btn-user-block').click(function(e){
        var id = $(this).data('id');
        if(confirm('Bạn có chắc muốn khoá tài khoản?')){
            $.ajax({
                type: 'POST',
                url: link_user_delete,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {
                    id: id,
                    _method: 'DELETE'
                },
                statusCode: {
                    204: function(){
                        $.notify({
                            message: 'Khoá tài khoản thành công.'
                        },{
                            type: 'success'
                        });
                        setTimeout(function(){
                            window.location.href = link_back
                        }, 1000);
                    }
                },
                success:function(res){
                    
                },
                error: function(e){
                    $.notify({
                        message: 'Không thể khoá tài khoản.'
                    },{
                        type: 'danger'
                    })
                }
            });
        }
        return false;
    })
});
