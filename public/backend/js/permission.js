$(function(){
    $('.btn-permission-admin').click(function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var item = $(this).data('item');
        var admin  = admins[item];
        
        $('input[type=checkbox]').each(function(){
            var per = $(this).attr('name');
            (admin.permissions.indexOf(per) > -1) ? this.checked = true
             : this.checked = false;
        })
        $('.btn-save-change-permission').data('id', id);
        $('.modal-admins').modal('toggle');
    });

    // Save change permission
    $('.btn-save-change-permission').click(function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var permissions = [];
        $('input[type=checkbox]').each(function(){
            if(this.checked){
                permissions.push($(this).val());
            }
        });
        $.ajax({
            type: 'POST',
            url: link_update_permission,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: {
                id: id,
                permissions: permissions,
                _method: 'PUT'
            },
            success:function(res){
                $('.modal-admins').modal('hide');
                $.notify({
                    message: 'Success.'
                }, {
                    type: 'success'
                });
                setTimeout(() => {
                    window.location.reload(1)
                }, 1500);
            },
            error: function(e){
                $.notify({
                    message: 'Có lỗi xảy ra.'
                }, {
                    type: 'danger'
                })
            }
        });
    })
})