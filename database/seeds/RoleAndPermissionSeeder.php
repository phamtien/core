<?php

use App\Core\Acl;
use App\Models\Permissions\Permission;
use App\Models\Roles\Role;
use Illuminate\Database\Seeder;

class RoleAndPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(Acl::roles() as $role){
            Role::create([
                'name' => $role,
                'slug' => str_slug($role)
            ]);
        }
        $adminRole = Role::where('name', Acl::ROLE_ADMIN)->first();
        foreach(Acl::permissions() as $permission){
            $per = Permission::create([
                'name' => $permission,
                'slug' => str_slug($permission)
            ]);
            $adminRole->permissions()->attach($per->id);
        }

    }
}
