<?php

use App\Core\Acl;
use App\Models\Admins\Admin;
use App\Models\Roles\Role;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleAndPermissionSeeder::class);
        // Create admin default
        $admin = Admin::create([
            'username' => 'admin',
            'email' => config('access.admin.email'),
            'password' => bcrypt(config('access.admin.password')),
            'admin' => true,
            'active' => true,
        ]);
        $role = Role::whereName(Acl::ROLE_ADMIN)->first();
        $admin->roles()->attach($role);
        factory(User::class, 50)->create();
    }
}
