<?php

/**
 * -----------------------------------
 * Routes for admin
 * -----------------------------------
 */

use App\Core\Acl;

Route::group([
    'namespace' => 'Backend',
], function(){
    /**
     * -------------------------------
     * Before login to dashboard
     * -------------------------------
     */
    Route::get('/{slug?}', 'HomeController@index')->name('admin.login')->where('slug', 'login');
    Route::post('login', 'HomeController@login')->name('admin.login');
    Route::get('logout', 'HomeController@logout')->name('admin.logout');
    Route::post('register', 'HomeController@register')->name('admin.register');

    /**
     * -------------------------------
     * After login to dashboard
     * -------------------------------
     */
    Route::group([
        'middleware' => ['admin'],
    ], function(){
        /**
         *  Dashboard manager
         */
        Route::group([
            'namespace' => 'Dashboard',
        ], function(){
            Route::get('index', 'DashboardController@index')->name('admin.dashboard');
        });
        Route::get('profile/{admin}', 'Admins\AdminController@profile')->name('admin.profile')->where('admin', '[0-9]+');

        /**
         * Administrator manage
         */
        Route::group([
            'namespace' => 'Users',
            'prefix' => 'users',
            'middleware' => ['permission'],
            'permissions' => [Acl::PERMISSION_USER_MANAGE],
            'roles' => [Acl::GUARD_ADMIN]
        ], function(){
            Route::get('/', 'UserController@index')->name('admin.users');
            Route::get('{user}', 'UserController@profile')->name('admin.users.profile')->where('user', '[0-9]+');
            Route::delete('/', 'UserController@delete')->name('admin.users.delete');
        });
        Route::group([
            'namespace' => 'Admins',
            'prefix' => 'admins',
            'middleware' => ['permission'],
            'permissions' => [Acl::PERMISSION_USER_MANAGE],
            'roles' => [Acl::GUARD_ADMIN]
        ], function(){
            Route::get('/', 'AdminController@index')->name('admin.admins');
            Route::put('active', 'AdminController@active')->name('admin.admins.active');
            Route::put('update-permission', 'AdminController@updatePermission')->name('admin.admins.updatePermission');
            Route::post('save-image-admin', 'AdminController@updateImageAdmin')->name('admin.admins.saveImage');
        });
    });
});