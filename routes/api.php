<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

/**
 * Routes for api version 1
 */
Route::group([
    'namespace' => 'Api\v1'
], function(){
    /**
     * Routes for users
     */
    Route::group([
        'namespace' => 'Users',
    ], function(){
        Route::post('login', 'UserController@login');
        Route::post('login/{provider}', 'UserController@loginWithSocial')->where('provider', '(facebook)|(google)');
        Route::post('register', 'UserController@register');
        Route::get('me', 'UserController@me');
        Route::get('logout', 'UserController@logout');

        Route::group([
            'prefix' => 'users'
        ], function(){
            Route::get('/', 'UserController@getUsers');
        });
    });
});
